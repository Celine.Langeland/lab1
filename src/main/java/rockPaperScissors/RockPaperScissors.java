package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);

    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String playerMove = getUserMove();
            String computerMove = getComputerMove();
        
            if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors")) {
        
                if (playerMove.equals(computerMove)) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!");
                    roundCounter += 1; 
                 }

                    else if (playerMove.equals("rock")) {
                        if (computerMove.equals("scissors")) {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                            humanScore += 1;
                            roundCounter += 1; 
                        }
                        if (computerMove.equals("paper"))  {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                            computerScore += 1;
                            roundCounter += 1; 
                        }
                    }

                    else if (playerMove.equals("scissors")) {
                        if (computerMove.equals("paper")) {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                            humanScore += 1;
                            roundCounter += 1; 
                        }

                        if (computerMove.equals("rock")) {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                            computerScore += 1;
                            roundCounter += 1; 
                        }
                    }

                    else if (playerMove.equals("paper")) {
                        if (computerMove.equals("rock")) {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                            humanScore += 1;
                            roundCounter += 1; 
                        }

                        if (computerMove.equals("scissors")) {
                            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                            computerScore += 1;
                            roundCounter += 1; 
                        }
                    }
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                String continue_answer = continue_playing();
                if (continue_answer.equals("n")) {
                    break;
                }
            }
            else
                System.out.println("I don't understand " + playerMove + ". Try again");
        }
    System.out.println("Bye bye :)");
    }
    public String getComputerMove() {
        String computerMove;
        Random random = new Random();
        
        int input = random.nextInt(3)+1;
        if (input == 1)
            computerMove = "rock";
        else if (input == 2)
            computerMove = "paper";
        else
            computerMove = "scissors";

        return computerMove;
    }

    public String getUserMove() {
        while (true) {
            String playerMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            return playerMove;  
        }
    }

    public String continue_playing() {
        while (true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            
            if (continue_answer.equals("y"))
                return continue_answer;
            else if (continue_answer.equals("n"))
                return continue_answer;
            else 
                System.out.println("I don't understand " + continue_answer + ". Try again");

        }
    }
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        return userInput;
    }

}
